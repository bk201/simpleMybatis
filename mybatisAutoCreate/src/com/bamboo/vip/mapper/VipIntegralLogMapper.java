package com.bamboo.vip.mapper;

import com.bamboo.vip.model.VipIntegralLog;

public interface VipIntegralLogMapper {
    int deleteByPrimaryKey(Long vipIntegralLogId);

    int insert(VipIntegralLog record);

    int insertSelective(VipIntegralLog record);

    VipIntegralLog selectByPrimaryKey(Long vipIntegralLogId);

    int updateByPrimaryKeySelective(VipIntegralLog record);

    int updateByPrimaryKey(VipIntegralLog record);
}