package com.bamboo.vip.mapper;

import com.bamboo.vip.model.VipBalanceLog;

public interface VipBalanceLogMapper {
    int deleteByPrimaryKey(Long vipBalanceLogId);

    int insert(VipBalanceLog record);

    int insertSelective(VipBalanceLog record);

    VipBalanceLog selectByPrimaryKey(Long vipBalanceLogId);

    int updateByPrimaryKeySelective(VipBalanceLog record);

    int updateByPrimaryKey(VipBalanceLog record);
}