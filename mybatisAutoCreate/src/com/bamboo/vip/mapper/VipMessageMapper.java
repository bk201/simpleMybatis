package com.bamboo.vip.mapper;

import com.bamboo.vip.model.VipMessage;

public interface VipMessageMapper {
    int deleteByPrimaryKey(Long vipMessageId);

    int insert(VipMessage record);

    int insertSelective(VipMessage record);

    VipMessage selectByPrimaryKey(Long vipMessageId);

    int updateByPrimaryKeySelective(VipMessage record);

    int updateByPrimaryKey(VipMessage record);
}