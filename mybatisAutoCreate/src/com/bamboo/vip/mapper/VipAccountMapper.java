package com.bamboo.vip.mapper;

import com.bamboo.vip.model.VipAccount;

public interface VipAccountMapper {
    int deleteByPrimaryKey(Long vipAccountId);

    int insert(VipAccount record);

    int insertSelective(VipAccount record);

    VipAccount selectByPrimaryKey(Long vipAccountId);

    int updateByPrimaryKeySelective(VipAccount record);

    int updateByPrimaryKey(VipAccount record);
}