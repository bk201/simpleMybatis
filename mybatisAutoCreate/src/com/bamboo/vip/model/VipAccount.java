package com.bamboo.vip.model;

import java.math.BigDecimal;
import java.util.Date;

public class VipAccount {
    private Long vipAccountId;

    private Long vipInfoId;

    private BigDecimal integral;

    private BigDecimal integralLastUse;

    private Date integralLastUseTime;

    private BigDecimal balancel;

    private BigDecimal balancelLastUse;

    private Date balancelLastUseTime;

    private Integer mobileIsValid;

    private Date mobileValidTime;

    private Integer emailIsValid;

    private Date emilValidTime;

    private String payPwd;

    private Date payPwdLastUpdateTime;

    private String memo;

    private Long optCounter;

    private String isDeleted;

    private Long createBy;

    private Date createDate;

    private Long updateBy;

    private Date updateDate;

    public Long getVipAccountId() {
        return vipAccountId;
    }

    public void setVipAccountId(Long vipAccountId) {
        this.vipAccountId = vipAccountId;
    }

    public Long getVipInfoId() {
        return vipInfoId;
    }

    public void setVipInfoId(Long vipInfoId) {
        this.vipInfoId = vipInfoId;
    }

    public BigDecimal getIntegral() {
        return integral;
    }

    public void setIntegral(BigDecimal integral) {
        this.integral = integral;
    }

    public BigDecimal getIntegralLastUse() {
        return integralLastUse;
    }

    public void setIntegralLastUse(BigDecimal integralLastUse) {
        this.integralLastUse = integralLastUse;
    }

    public Date getIntegralLastUseTime() {
        return integralLastUseTime;
    }

    public void setIntegralLastUseTime(Date integralLastUseTime) {
        this.integralLastUseTime = integralLastUseTime;
    }

    public BigDecimal getBalancel() {
        return balancel;
    }

    public void setBalancel(BigDecimal balancel) {
        this.balancel = balancel;
    }

    public BigDecimal getBalancelLastUse() {
        return balancelLastUse;
    }

    public void setBalancelLastUse(BigDecimal balancelLastUse) {
        this.balancelLastUse = balancelLastUse;
    }

    public Date getBalancelLastUseTime() {
        return balancelLastUseTime;
    }

    public void setBalancelLastUseTime(Date balancelLastUseTime) {
        this.balancelLastUseTime = balancelLastUseTime;
    }

    public Integer getMobileIsValid() {
        return mobileIsValid;
    }

    public void setMobileIsValid(Integer mobileIsValid) {
        this.mobileIsValid = mobileIsValid;
    }

    public Date getMobileValidTime() {
        return mobileValidTime;
    }

    public void setMobileValidTime(Date mobileValidTime) {
        this.mobileValidTime = mobileValidTime;
    }

    public Integer getEmailIsValid() {
        return emailIsValid;
    }

    public void setEmailIsValid(Integer emailIsValid) {
        this.emailIsValid = emailIsValid;
    }

    public Date getEmilValidTime() {
        return emilValidTime;
    }

    public void setEmilValidTime(Date emilValidTime) {
        this.emilValidTime = emilValidTime;
    }

    public String getPayPwd() {
        return payPwd;
    }

    public void setPayPwd(String payPwd) {
        this.payPwd = payPwd == null ? null : payPwd.trim();
    }

    public Date getPayPwdLastUpdateTime() {
        return payPwdLastUpdateTime;
    }

    public void setPayPwdLastUpdateTime(Date payPwdLastUpdateTime) {
        this.payPwdLastUpdateTime = payPwdLastUpdateTime;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public Long getOptCounter() {
        return optCounter;
    }

    public void setOptCounter(Long optCounter) {
        this.optCounter = optCounter;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted == null ? null : isDeleted.trim();
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}