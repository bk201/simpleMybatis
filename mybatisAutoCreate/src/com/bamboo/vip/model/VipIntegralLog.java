package com.bamboo.vip.model;

import java.math.BigDecimal;
import java.util.Date;

public class VipIntegralLog {
    private Long vipIntegralLogId;

    private Long vipInfoId;

    private Long orderId;

    private String orderCode;

    private Integer integeralType;

    private Integer actionType;

    private BigDecimal oldIntegral;

    private BigDecimal integral;

    private BigDecimal newIntegral;

    private String source;

    private String remarks;

    private Integer isView;

    private String memo;

    private Long optCounter;

    private String isDeleted;

    private Long createBy;

    private Date createDate;

    private Long updateBy;

    private Date updateDate;

    public Long getVipIntegralLogId() {
        return vipIntegralLogId;
    }

    public void setVipIntegralLogId(Long vipIntegralLogId) {
        this.vipIntegralLogId = vipIntegralLogId;
    }

    public Long getVipInfoId() {
        return vipInfoId;
    }

    public void setVipInfoId(Long vipInfoId) {
        this.vipInfoId = vipInfoId;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode == null ? null : orderCode.trim();
    }

    public Integer getIntegeralType() {
        return integeralType;
    }

    public void setIntegeralType(Integer integeralType) {
        this.integeralType = integeralType;
    }

    public Integer getActionType() {
        return actionType;
    }

    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    public BigDecimal getOldIntegral() {
        return oldIntegral;
    }

    public void setOldIntegral(BigDecimal oldIntegral) {
        this.oldIntegral = oldIntegral;
    }

    public BigDecimal getIntegral() {
        return integral;
    }

    public void setIntegral(BigDecimal integral) {
        this.integral = integral;
    }

    public BigDecimal getNewIntegral() {
        return newIntegral;
    }

    public void setNewIntegral(BigDecimal newIntegral) {
        this.newIntegral = newIntegral;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source == null ? null : source.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public Integer getIsView() {
        return isView;
    }

    public void setIsView(Integer isView) {
        this.isView = isView;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public Long getOptCounter() {
        return optCounter;
    }

    public void setOptCounter(Long optCounter) {
        this.optCounter = optCounter;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted == null ? null : isDeleted.trim();
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}