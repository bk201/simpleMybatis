package com.bamboo.vip.model;

import java.util.Date;

public class VipMessage {
    private Long vipMessageId;

    private Long vipInfoId;

    private String title;

    private String contnets;

    private Integer isSee;

    private String memo;

    private Long optCounter;

    private String isDeleted;

    private Long createBy;

    private Date createDate;

    private Long updateBy;

    private Date updateDate;

    public Long getVipMessageId() {
        return vipMessageId;
    }

    public void setVipMessageId(Long vipMessageId) {
        this.vipMessageId = vipMessageId;
    }

    public Long getVipInfoId() {
        return vipInfoId;
    }

    public void setVipInfoId(Long vipInfoId) {
        this.vipInfoId = vipInfoId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title == null ? null : title.trim();
    }

    public String getContnets() {
        return contnets;
    }

    public void setContnets(String contnets) {
        this.contnets = contnets == null ? null : contnets.trim();
    }

    public Integer getIsSee() {
        return isSee;
    }

    public void setIsSee(Integer isSee) {
        this.isSee = isSee;
    }

    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo == null ? null : memo.trim();
    }

    public Long getOptCounter() {
        return optCounter;
    }

    public void setOptCounter(Long optCounter) {
        this.optCounter = optCounter;
    }

    public String getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(String isDeleted) {
        this.isDeleted = isDeleted == null ? null : isDeleted.trim();
    }

    public Long getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Long createBy) {
        this.createBy = createBy;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Long getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Long updateBy) {
        this.updateBy = updateBy;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }
}