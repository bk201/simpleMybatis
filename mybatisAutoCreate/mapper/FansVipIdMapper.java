package mapper;

import model.FansVipId;

public interface FansVipIdMapper {
    int insert(FansVipId record);

    int insertSelective(FansVipId record);
}