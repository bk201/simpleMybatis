package model;

public class UserInfo {
    private Integer id;

    private String name;

    private Integer passname;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getPassname() {
        return passname;
    }

    public void setPassname(Integer passname) {
        this.passname = passname;
    }
}