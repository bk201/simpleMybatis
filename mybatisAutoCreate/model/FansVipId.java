package model;

public class FansVipId {
    private Long vipInfoId;

    private String openId;

    public Long getVipInfoId() {
        return vipInfoId;
    }

    public void setVipInfoId(Long vipInfoId) {
        this.vipInfoId = vipInfoId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }
}