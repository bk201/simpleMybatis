package com.bamboo.message.mapper;

import com.bamboo.message.model.WscMessageInfo;

public interface WscMessageInfoMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WscMessageInfo record);

    int insertSelective(WscMessageInfo record);

    WscMessageInfo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WscMessageInfo record);

    int updateByPrimaryKey(WscMessageInfo record);
}