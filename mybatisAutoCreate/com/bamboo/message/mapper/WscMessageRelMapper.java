package com.bamboo.message.mapper;

import com.bamboo.message.model.WscMessageRel;

public interface WscMessageRelMapper {
    int deleteByPrimaryKey(Long id);

    int insert(WscMessageRel record);

    int insertSelective(WscMessageRel record);

    WscMessageRel selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(WscMessageRel record);

    int updateByPrimaryKey(WscMessageRel record);
}